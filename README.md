```sh
git clone https://leyht@bitbucket.org/leyht/sometulorestinterfaceprototypestuff.git
cd sometulorestinterfaceprototypestuff
python3 -m venv venv
source venv/bin/activate
pip install django djangorestframework
python manage.py migrate
python manage.py runserver 0.0.0.0:8000
```

Damit klont man den Code, 
kreiert eine virtuelle Umgebung (damit man mit Django nicht global installieren muss),
mit `migrate` füllt man die Datenbank
und dann startet man den Development Server, sodass man auch über das Netzwerk Zugriff hat
(muss man halt schauen, wie dann die lokale IP des Rechners ist).

Folgende Endpoints sind definiert:

* `/pickup/`
* `/bags/`

Optional kann man noch `python manage.py createsuperuser` aufrufen und über den `/admin/`-Endpoint User anlegen,
aber da fehlt dann noch die Logik hinter einem Login
(wenn das auch einfach zu verknüpfen wäre, weil genau das ein Beispiel in der offiziellen Dokumentation ist).