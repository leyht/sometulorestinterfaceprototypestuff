from .models import User, Location, ShoppingBag, Transaction
from .serializers import LocationSerializer, UserSerializer, ShoppingBagSerializer, TransactionSerializer
from .serializers import CreateBagSerializer
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework import status

# Create your views here.


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class ShoppingBagViewSet(viewsets.ModelViewSet):
    queryset = ShoppingBag.objects.all()
    serializer_class = ShoppingBagSerializer

class TransactionViewSet(viewsets.ModelViewSet):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer

class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

# HERE COMES THE REAL INTERFACE

class PickupViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Location.objects.filter(isCollectionPoint=True)
    serializer_class = LocationSerializer

class CreateBag(APIView):
    def post(self, request, format=None):
        data = JSONParser().parse(request)
        serializer = CreateBagSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response("OK", status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
