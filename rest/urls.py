from django.conf.urls import url, include
from rest_framework import routers
from .views import UserViewSet, ShoppingBagViewSet, TransactionViewSet, LocationViewSet
from .views import PickupViewSet, CreateBag


router = routers.DefaultRouter()
router.register(r'pickup', PickupViewSet)

router.register(r'users', UserViewSet)
router.register(r'shoppingbag', ShoppingBagViewSet)
router.register(r'transactions', TransactionViewSet)
router.register(r'locations', LocationViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^bags/$', CreateBag.as_view()),
]
