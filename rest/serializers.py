from .models import User, Location, ShoppingBag, Transaction
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'role')

class ShoppingBagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ShoppingBag
        fields = ('qrCode', 'customer', 'target')

class TransactionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Transaction
        fields = ('is_accepted', 'user', 'shopping_bag', "created_at")

# HERE COMES THE REAL INTERFACE

class LocationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Location
        fields = ('id', 'name', 'latitude', 'longitude', 'isCollectionPoint')

class CreateBagSerializer(serializers.Serializer):
    customer = serializers.CharField(max_length=200)
    bags = serializers.ListField(
        child=serializers.CharField(max_length=200)
    )
    target = serializers.IntegerField()

    def create(self, validated_data):
        target = Location.objects.get(id=validated_data["target"])
        customer = validated_data["customer"]
        bags = [ShoppingBag(qrCode=qrCode,
                            customer=customer,
                            target=target)
                for qrCode in validated_data["bags"]]
        vendor = User.objects.filter(location__id=target.id)[0]
        for bag in bags:
            bag.save()
            transaction = Transaction(shopping_bag=bag, user=vendor, is_accepted=True)
            transaction.save()
        return bag
