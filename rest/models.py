from django.db import models


class Location(models.Model):
    name = models.CharField(max_length=200)
    latitude = models.FloatField()
    longitude = models.FloatField()
    isCollectionPoint = models.BooleanField()


class User(models.Model):
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    role = models.CharField(max_length=200)
    location = models.ForeignKey(Location, on_delete=models.CASCADE, null=True)


class ShoppingBag(models.Model):
    qrCode = models.CharField(max_length=200)
    customer = models.CharField(max_length=200)
    target = models.ForeignKey(Location, on_delete=models.CASCADE)


class Transaction(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    is_accepted = models.BooleanField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    shopping_bag = models.ForeignKey(ShoppingBag, on_delete=models.CASCADE)
